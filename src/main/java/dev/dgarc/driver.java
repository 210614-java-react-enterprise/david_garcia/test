package dev.dgarc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.dgarc.models.News;

import javax.annotation.processing.Completion;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class driver {


    public static void main(String[] args) throws InterruptedException, IOException, ExecutionException {
//        String apiKey = "f97cb0580ef44bc49cbaf22a6c6fcf70";
//        String targetUrl = "https://newsapi.org/v2/everything?q=stocks&from=2021-08-09&to=2021-08-09&sortBy=popularity&domains=forbes.com&apiKey=f97cb0580ef44bc49cbaf22a6c6fcf70&pageSize=5&page=1";
//        // ?category=business&country=us&apiKey=f97cb0580ef44bc49cbaf22a6c6fcf70
//        String urlParameters = "";
//
//
//        UncheckedObjectMapper objectMapper = new UncheckedObjectMapper();
//
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create(targetUrl))
//                .header("Accept","application/json")
//                .build();
//
//        CompletableFuture<Map<String,String>> httpMap =
//                HttpClient.newHttpClient().sendAsync(request, HttpResponse.BodyHandlers.ofString())
//                    .thenApply(HttpResponse::body)
//                    .thenApply(objectMapper::readValue);
//
//        /*System.out.println(httpMap.get());*/
//
//        Set<Map.Entry<String,String>> httpSet = httpMap.get().entrySet();
//        System.out.println(httpSet.stream().filter(x -> x.getKey().equals("articles")).collect(Collectors.toList()).toString());

        // create a Json string which Jackson can deserialize into my News object
        String eventualJsonString = "{\"title\":\"Fake Title!\"," +
                "\"description\":\"Fake Description!\"," +
                "\"url\":\"Fake URL!\"," +
                "\"urlToImage\":\"Fake img URL!\"}";

        ObjectMapper objectMapper = new ObjectMapper();

        News news = objectMapper.readValue(eventualJsonString, News.class);
        System.out.println(news.toString());
    }
}

class UncheckedObjectMapper extends ObjectMapper {
    // parse given JSON string into a map
    Map<String,String> readValue(String content){
        try{
            return this.readValue(content, new TypeReference<>(){});
        }catch (IOException ioe){
            throw new CompletionException(ioe);
        }
    }
}
